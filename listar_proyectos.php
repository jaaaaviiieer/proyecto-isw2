<?php 
    include 'conexion.php';
    $query="SELECT * FROM proyectos";
    $consulta_proyectos = $conexion->query($query);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Lista Proyectos</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="css/styles.css">
    
</head>
<body>
<div class="row justify-content-center align-items-center vh-100">
    <div class="col-9 formulario">


    <div class="contenedor">
        <div class="table-responsive" style="padding: 1%">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th scope="col">Id</th>
                        <th scope="col">Nombre</th>
                        <th scope="col">Cliente</th>
                        <th scope="col">Fecha Inicio</th>
                        <th scope="col">Fecha Fin</th>
                        <th scope="col">Eliminar/Modificar</th>
                        
                    </tr>
                </thead>
                <tbody>
                    <?php
                        if($consulta_proyectos->num_rows >0){
                            while($lb = $consulta_proyectos->fetch_assoc()){

                    ?>
                    <tr>
                        <td><?php echo $lb['id'] ?></td>
                        <td><?php echo $lb['nombre'] ?></td>
                        <td><?php echo $lb['cliente'] ?></td>
                        <td><?php echo $lb['inicio'] ?></td>
                        <td><?php echo $lb['fin'] ?></td>
                        <td>
                            <a type ="Eliminar" class="btn btn-danger m-r-1em" href=<?php echo "eliminar_proyectos.php?id=" . $lb['id']?>>Eliminar</a>
                            <a type ="Editar" class='btn btn-warning m-r-1em' href=<?php echo "editar_proyectos.php?id=" . $lb['id']?>>Editar</a>
                        </td>
                    </tr>
                    
                    <?php }} ?>
                </tbody>
            </table>
        </div>
    </div>    
    <div>
        <form action="proyectos.php">
        <p align="center"><button type="submit" class="btn btn-secondary"> Agregar Proyecto </p>
        </form>
    </div>
    <div>
        <form action="index.php">
        <p align="center"><button type="submit" class="btn btn-secondary"> Menu </p>
        </form>
    </div>
</body>

