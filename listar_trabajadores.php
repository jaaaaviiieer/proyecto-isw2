<?php 
    include 'conexion.php';
    $query="SELECT * FROM trabajadores";
    $consulta_trabajadores = $conexion->query($query);
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Lista trabajadores</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="css/styles.css">
    
</head>
<body>
<div class="row justify-content-center align-items-center vh-100">
    <div class="col-9 formulario">
    <div class="contenedor">
        <div class="table-responsive" style="padding: 1%">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th scope="col">RUT</th>
                        <th scope="col">Nombre</th>
                        <th scope="col">Apellidos</th>
                        <th scope="col">Correo</th>
                        <th scope="col">Telefono</th>
                        <th scope="col">Eliminar/Modificar</th>
                        
                    </tr>
                </thead>
                <tbody>
                    <?php
                        if($consulta_trabajadores->num_rows >0){
                            while($lb = $consulta_trabajadores->fetch_assoc()){

                    ?>
                    <tr>
                        <td><?php echo $lb['rut'] ?></td>
                        <td><?php echo $lb['nombre'] ?></td>
                        <td><?php echo $lb['apellidos'] ?></td>
                        <td><?php echo $lb['correo'] ?></td>
                        <td><?php echo $lb['telefono'] ?></td>
                        <td>
                            <a type ="Eliminar" class="btn btn-danger m-r-1em" href=<?php echo "eliminar_trabajador.php?rut=" . $lb['rut']?>>Eliminar</a>
                            <a type ="Editar" class='btn btn-warning m-r-1em' href=<?php echo "editar_trabajador.php?rut=" . $lb['rut']?>>Editar</a>
                        </td>
                    </tr>
                    
                    <?php }} ?>
                </tbody>
            </table>
        </div>
    </div>    
    <div>
        <form action="trabajadores.php">
        <p align="center"><button type="submit" class="btn btn-secondary"> Agregar Trabajador </p>
        </form>
    </div>
    <div>
        <form action="index.php">
        <p align="center"><button type="submit" class="btn btn-secondary"> Menu </p>
        </form>
    </div>
</body>

