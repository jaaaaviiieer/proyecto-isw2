<?php 
    include 'conexion.php';

    $query = mysqli_fetch_array(mysqli_query($conexion,"SELECT MAX(id) FROM cuadrillas"));
    $last = end($query);
    $consulta_trabajadores = mysqli_query($conexion,"SELECT * FROM trabajadores WHERE cuadrilla=0");
?>
<?php
include 'conexion.php';
if(isset($_POST['save'])){
	$checkbox = $_POST['check'];
	for($i=0;$i<count($checkbox);$i++){ 
        $del_id = $checkbox[$i]; 
        $update = "UPDATE trabajadores SET cuadrilla='$last' WHERE rut='$del_id'";
        $resultado = $conexion->query($update);
        if($resultado==1){
            header('Location: cuadrillas.php');
        }
    }
}
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Agregar Trabajadores a Cuadrilla</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="css/styles.css">
</head>
<body>
    <div><?php if(isset($message)) { echo $message; } ?>
    </div>
    <form method="post" action="">
    <table class="table table-bordered">
    <thead>
        <tr>
            <th>Seleccion</th>
            <th>Rut</th>
	        <th>Nombre</th>
	        <th>Apellidos</th>
        </tr>
    </thead>
<?php
    if($consulta_trabajadores->num_rows >0){
    while($lb = mysqli_fetch_array($consulta_trabajadores)){
?>
    <tr>
        <td><input type="checkbox" id="checkItem" name="check[]" value="<?php echo $lb["rut"]; ?>"></td>
        <td><?php echo $lb["rut"]; ?></td>
	    <td><?php echo $lb["nombre"]; ?></td>
	    <td><?php echo $lb["apellidos"]; ?></td>
    </tr>
<?php }} ?>
</table>
    <p align="center"><button type="submit" class="btn btn-success" name="save">Agregar</button></p>
    </form>
    <script>
        $("#checkAl").click(function () {
        $('input:checkbox').not(this).prop('checked', this.checked);
        });
    </script>
<div class="botones">
    <a href="cuadrillas.php"> Agregar cuadrilla </a>
</div>
</body>
</html>