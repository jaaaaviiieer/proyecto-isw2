<?php 
    include 'conexion.php';
    $id2 = $_GET['id'];
    $query = mysqli_fetch_array(mysqli_query($conexion,"SELECT id FROM cuadrillas WHERE id='$id2'"));
    $last = end($query);
    $consulta_trabajadores = mysqli_query($conexion,"SELECT * FROM trabajadores WHERE cuadrilla='$id2'");
    $consulta_trabajadores2 = mysqli_query($conexion,"SELECT * FROM trabajadores WHERE cuadrilla=0");
?>
<?php
include 'conexion.php';
if(isset($_POST['save'])){
	$checkbox = $_POST['check'];
	for($i=0;$i<count($checkbox);$i++){ 
        $del_id = $checkbox[$i]; 
        $update = "UPDATE trabajadores SET cuadrilla='$last' WHERE rut='$del_id'";
        $resultado = $conexion->query($update);
        if($resultado==1){
            header('Location: cuadrillas.php');
        }
    }
}
?>
<?php
include 'conexion.php';
if(isset($_POST['notsave'])){
	$checkbox = $_POST['check'];
	for($i=0;$i<count($checkbox);$i++){ 
        $del_id = $checkbox[$i]; 
        $update = "UPDATE trabajadores SET cuadrilla=0 WHERE rut='$del_id'";
        $resultado = $conexion->query($update);
        if($resultado==1){
            header('Location: cuadrillas.php');
        }
    }
}
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Agregar Trabajadores a Cuadrilla</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="css/styles.css">
</head>
<body>
    <div><?php if(isset($message)) { echo $message; } ?>
    </div>
    <form method="post" action="">
    <h2 class="text-center">Trabajadores pertenecientes a cuadrilla</h2>
    <div>
    <table class="table table-bordered">
    <thead>
        <tr>
            <th>Seleccion</th>
            <th>Rut</th>
	        <th>Nombre</th>
	        <th>Apellidos</th>
        </tr>
    </thead>
<?php
    if($consulta_trabajadores->num_rows >0){
    while($lb = mysqli_fetch_array($consulta_trabajadores)){
?>
    <tr>
        <td><input type="checkbox" id="checkItem" name="check[]" value="<?php echo $lb["rut"]; ?>"></td>
        <td><?php echo $lb["rut"]; ?></td>
	    <td><?php echo $lb["nombre"]; ?></td>
	    <td><?php echo $lb["apellidos"]; ?></td>
    </tr>
<?php }} ?>
    </table>
    <p align="center"><button type="submit" class="btn btn-danger m-r-1em" name="notsave">Eliminar</button></p>
    </form>
</div>
    <form method="post" action="">
    <h2 class="text-center">Trabajadores sin cuadrilla asignada</h2>
    <table class="table table-bordered">
    <thead>
        <tr>
            <th>Seleccion</th>
            <th>Rut</th>
	        <th>Nombre</th>
	        <th>Apellidos</th>
        </tr>
    </thead>
<?php
    if($consulta_trabajadores2->num_rows >0){
    while($lb = mysqli_fetch_array($consulta_trabajadores2)){
?>
    <tr>
        <td><input type="checkbox" id="checkItem" name="check[]" value="<?php echo $lb["rut"]; ?>"></td>
        <td><?php echo $lb["rut"]; ?></td>
	    <td><?php echo $lb["nombre"]; ?></td>
	    <td><?php echo $lb["apellidos"]; ?></td>
    </tr>
<?php }} ?>
    </table>
    <p align="center"><button type="submit" class="btn btn-success" name="save">Agregar</button></p>
    </form>
    <script>
        $("#checkAl").click(function () {
        $('input:checkbox').not(this).prop('checked', this.checked);
        });
    </script>
<div class="botones">
    <a href="cuadrillas.php"> Agregar cuadrilla </a>
</div>
</body>
</html>