<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>Agregar Proyectos</title>
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
	<link rel="stylesheet" href="css/styles.css">
</head>
<body>
	
	<div class="container">
			
			<div class="row justify-content-center align-items-center vh-100">
				<div class="d-flex h-75">
					<div class="col-3 imagen">
					</div>
					
						<div class="col-9 formulario">
							<h2 class="text-center">Registro de Proyectos</h2>
							<h6 class="text-center">Este formulario permite registrar Proyectos al sistema</h4>
							<form action="agregar_proyecto.php" method="POST" multipart="" enctype="multipart/form-data">
								<div class="container">
									<div class="row row-cols-2">
										<div class="col form-group">
											<label for=""><b>Nombre Proyecto</b></label>
											<input type="text" class="form-control" name="nombre" placeholder="Ingrese nombre del Proyecto" required>
										</div>
										<div class="col form-group">
											<label for=""><b>Cliente Proyecto</b></label>
											<input type="text" class="form-control" name="cliente" placeholder="Ingrese nombre del Cliente" required>
										</div>
                                        <div class="col form-group">
											<label for=""><b>Duracion</b></label>
											<p>Inicio proyecto <input type="date" class="form-control" name="duracion" required></p>
                                            <p>Fin Proyecto <input type="date" class="form-control" name="duracion2" required></p>
										</div>
										<div class="col form-group">
											<label for=""><b>Documentos del Proyecto</b></label>
											<input type="file" class="form-control" name="archivo[]" multiple >
										</div>
									</div>
									<button type="submit" class="btn btn-secondary btn-lg btn-block">Registrar</button>

								</div>
							</form>
							<br>
							<form action="listar_proyectos.php" >
								<div class="col form-group">
									<button type="submit" class="btn btn-secondary btn-lg btn-block">Listar Proyectos</button>
								</div>
							</form>


						</div>
					
				</div>
			</div>
		
	</div>

	<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
</body>
</html>